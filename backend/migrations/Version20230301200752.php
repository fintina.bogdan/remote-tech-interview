<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230301200752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE price_range (id INT AUTO_INCREMENT NOT NULL, product_variation_id INT NOT NULL, shape VARCHAR(255) NOT NULL, base_price DOUBLE PRECISION NOT NULL, sell_price DOUBLE PRECISION NOT NULL, buy_price DOUBLE PRECISION NOT NULL, INDEX IDX_B84E5229DC269DB3 (product_variation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE price_range ADD CONSTRAINT FK_B84E5229DC269DB3 FOREIGN KEY (product_variation_id) REFERENCES product_variation (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE price_range DROP FOREIGN KEY FK_B84E5229DC269DB3');
        $this->addSql('DROP TABLE price_range');
    }
}
