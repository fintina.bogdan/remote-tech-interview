<?php

namespace App\Service;

use App\Dto\CalculatePriceDto;
use App\Entity\PriceRange;
use App\Entity\Product;
use App\Entity\ProductVariation;
use Doctrine\Persistence\ManagerRegistry;
use RuntimeException;

class PriceService
{
    public function __construct(private readonly ManagerRegistry $registry)
    {
    }

    public function calculatePrice(CalculatePriceDto $calculatePriceDto): float
    {
        $priceRangesToCalculate = [];
        $calculatePriceDto = $this->normalizeShape($calculatePriceDto);
        $priceRangeRepository = $this->registry->getManager()->getRepository(PriceRange::class);

        $priceRanges = $priceRangeRepository->findBy([
            'productVariation' => $this->findProductVariation($calculatePriceDto)
        ]);
        /** @var PriceRange $priceRange */
        foreach($priceRanges as $priceRange) {
            if($priceRange->getShape() === $calculatePriceDto->display || $priceRange->getShape() === $calculatePriceDto->back) {
                $priceRangesToCalculate[] = $priceRange->getSellPrice();
            }
        }
        if(count($priceRangesToCalculate) === 0) {
            throw new RuntimeException("No price found for the provided conditions");
        }

        return array_sum($priceRangesToCalculate) / count($priceRangesToCalculate);
    }

    private function normalizeShape(CalculatePriceDto $calculatePriceDto): CalculatePriceDto
    {
        $calculatePriceDto->back = strtoupper(str_replace(" ", "_", $calculatePriceDto->back));
        $calculatePriceDto->display = strtoupper(str_replace(" ", "_", $calculatePriceDto->display));

        return $calculatePriceDto;
    }

    private function findProductVariation(CalculatePriceDto $calculatePriceDto): ProductVariation
    {
        $productRepository = $this->registry->getManager()->getRepository(Product::class);
        $variationRepository = $this->registry->getManager()->getRepository(ProductVariation::class);

        $product = $productRepository->find((int)$calculatePriceDto->product);
        if (!$product instanceof Product) {
            throw new RuntimeException(sprintf("Product with id %s was not found", $calculatePriceDto->product));
        }
        $productVariation = $variationRepository->findOneBy([
            'product' => $product,
            'color' => $calculatePriceDto->color,
            'storage' => (float)$calculatePriceDto->storage
        ]);

        if (!$productVariation instanceof ProductVariation) {
            throw new RuntimeException(sprintf("No product variation found for %s", $calculatePriceDto->product));
        }
        return $productVariation;
    }
}