<?php

namespace App\DataFixtures;

use App\Entity\PriceRange;
use App\Entity\Product;
use App\Entity\ProductVariation;
use App\Repository\PriceRangeRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductVariationRepository;
use App\Util\ProductVariationTrait;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Yaml\Parser;

class AppFixtures extends Fixture
{
    use ProductVariationTrait;

    public function __construct(
        private readonly ProductRepository $productsRepository,
        private readonly ProductVariationRepository $productVariationsRepository,
        private readonly PriceRangeRepository $priceRangeRepository,
    )
    {
    }

    public function load(ObjectManager $manager): void
    {
        $productsData = (new Parser())->parse(file_get_contents(__DIR__."/products.json"));

        foreach($productsData as $productData) {
            $productData['storage'] = $this->normalizeStorage($productData['storage']);

            $product = $this->getOrCreateProduct($productData);
            $productVariation = $this->getOrCreateProductVariation($productData, $product);

            $priceRange = $this->priceRangeRepository->findOneBy([
                'productVariation' => $productVariation,
                'shape' => $productData['shape']
            ]);

            if(!$priceRange instanceof PriceRange) {
                $priceRange = (new PriceRange())->setProductVariation($productVariation)
                    ->setBasePrice($productData['base_price'])
                    ->setBuyPrice($productData['buy_price'])
                    ->setSellPrice($productData['sell_price'])
                    ->setShape($productData['shape']);
                $this->priceRangeRepository->save($priceRange, true);
            }
        }
    }

    private function getOrCreateProduct(array $productData): Product
    {
        $product = $this->productsRepository->findOneBy([
            'model' => $productData['model'],
            'brand' => $productData['brand']
        ]);

        if(!$product instanceof Product) {
            $product = (new Product())->setModel($productData['model'])
                ->setBrand($productData['brand']);
            $this->productsRepository->save($product, true);
        }
        return $product;
    }

    private function getOrCreateProductVariation(array $productData, Product $product): ProductVariation
    {
        $productVariation = $this->productVariationsRepository->findOneBy([
            'product' => $product,
            'color' => $productData['color'],
            'storage' => $productData['storage'],
        ]);

        if(!$productVariation instanceof ProductVariation) {
            $productVariation = (new ProductVariation())->setProduct($product)
                ->setColor($productData['color'])
                ->setStorage($productData['storage']);
            $this->productVariationsRepository->save($productVariation, true);
        }
        return $productVariation;
    }
}
