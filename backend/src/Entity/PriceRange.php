<?php

namespace App\Entity;

use App\Repository\PriceRangeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PriceRangeRepository::class)]
class PriceRange
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'priceRanges')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ProductVariation $productVariation = null;

    #[ORM\Column(length: 255)]
    private ?string $shape = null;

    #[ORM\Column]
    private ?float $basePrice = null;

    #[ORM\Column]
    private ?float $sellPrice = null;

    #[ORM\Column]
    private ?float $buyPrice = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductVariation(): ?ProductVariation
    {
        return $this->productVariation;
    }

    public function setProductVariation(?ProductVariation $productVariation): self
    {
        $this->productVariation = $productVariation;

        return $this;
    }

    public function getShape(): ?string
    {
        return $this->shape;
    }

    public function setShape(string $shape): self
    {
        $this->shape = $shape;

        return $this;
    }

    public function getBasePrice(): ?float
    {
        return $this->basePrice;
    }

    public function setBasePrice(float $basePrice): self
    {
        $this->basePrice = $basePrice;

        return $this;
    }

    public function getSellPrice(): ?float
    {
        return $this->sellPrice;
    }

    public function setSellPrice(float $sellPrice): self
    {
        $this->sellPrice = $sellPrice;

        return $this;
    }

    public function getBuyPrice(): ?float
    {
        return $this->buyPrice;
    }

    public function setBuyPrice(float $buyPrice): self
    {
        $this->buyPrice = $buyPrice;

        return $this;
    }
}
