<?php

namespace App\Entity;

use App\Repository\ProductVariationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProductVariationRepository::class)]
class ProductVariation
{
    #[Groups(groups: "product_variation")]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(groups: "product_variation")]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $color = null;

    #[Groups(groups: "product_variation")]
    #[ORM\Column(nullable: true)]
    private ?int $storage = null;

    #[Groups(groups: "product_price")]
    #[ORM\OneToMany(mappedBy: 'productVariation', targetEntity: PriceRange::class, orphanRemoval: true)]
    private Collection $priceRanges;

    #[Groups(groups: "inverse_product")]
    #[ORM\ManyToOne(inversedBy: 'productVariations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $product = null;

    public function __construct()
    {
        $this->priceRanges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getStorage(): ?int
    {
        return $this->storage;
    }

    public function setStorage(?int $storage): self
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * @return Collection<int, PriceRange>
     */
    public function getPriceRanges(): Collection
    {
        return $this->priceRanges;
    }

    public function addPriceRange(PriceRange $priceRange): self
    {
        if (!$this->priceRanges->contains($priceRange)) {
            $this->priceRanges->add($priceRange);
            $priceRange->setProductVariation($this);
        }

        return $this;
    }

    public function removePriceRange(PriceRange $priceRange): self
    {
        if ($this->priceRanges->removeElement($priceRange)) {
            // set the owning side to null (unless already changed)
            if ($priceRange->getProductVariation() === $this) {
                $priceRange->setProductVariation(null);
            }
        }

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
