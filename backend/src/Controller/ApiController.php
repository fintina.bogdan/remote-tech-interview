<?php

namespace App\Controller;

use App\Dto\CalculatePriceDto;
use App\Entity\Product;
use App\Service\PriceService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ApiController extends AbstractController
{
    public function __construct(
        private readonly ManagerRegistry $registry
    )
    {
    }

    #[Route(path: "/products", name: "products.list", methods: [Request::METHOD_GET])]
    public function listProducts(): JsonResponse
    {
        $productRepository = $this->registry->getManager()->getRepository(Product::class);
        $products = $productRepository->findAll();

        return $this->json(
            $products,
            context: ['groups' => ['product', 'product_variation']]
        );
    }

    #[Route(path: "/product/price/calculate", name: "products.price.calculate", methods: [Request::METHOD_POST])]
    public function calculatePrice(Request $request, PriceService $priceService): JsonResponse
    {
        $serializer = new Serializer([new ObjectNormalizer()], [ new JsonEncoder()]);
        try {
            $calculatePriceDto = $serializer->deserialize($request->getContent(), CalculatePriceDto::class, 'json');
            $price = $priceService->calculatePrice($calculatePriceDto);

            return $this->json(['price' => $price]);
        } catch (\Throwable $exception) {
            return $this->json($exception->getMessage(), 400);
        }
    }
}