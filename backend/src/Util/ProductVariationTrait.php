<?php

namespace App\Util;


trait ProductVariationTrait
{
    public function normalizeStorage(string $storage): float
    {
        $storageDetails = explode(" ", $storage);
        if(count($storageDetails) !== 2) {
            throw new \RuntimeException(sprintf("Storage string contains %s arguments", count($storageDetails)));
        }
        return match (end($storageDetails)) {
            "GB" => $storageDetails[0],
            "TB" => $storageDetails[0] * 1024,
            "MB" => $storageDetails[0] / 1000,
            default => throw new \RuntimeException(sprintf("Storage type %s not recognised", end($storageDetails))),
        };
    }
}