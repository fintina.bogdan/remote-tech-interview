<?php

namespace App\Dto;

class CalculatePriceDto
{
    public string $storage;
    public string $color;
    public string $product;
    public string $network;
    public string $back;
    public string $display;
    public string $functional;
    public string $liquids;
}