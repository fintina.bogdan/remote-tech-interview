.DEFAULT_GOAL := help

help:
	@grep -E '(^[a-zA-Z0-9_-]+:.?##.$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

up: ##build and start docker containers
	docker-compose build && docker-compose up -d
	docker-compose exec php-fpm php composer.phar install
	docker-compose exec php-fpm php bin/console doc:database:create --if-not-exists
	docker-compose exec php-fpm php bin/console doc:mig:mig -n -q
	docker-compose exec php-fpm php bin/console doc:fix:load -q

stop: ## Stop docker containers
	docker-compose stop

destroy: ## Stop and destroy docker containers
	make stop
	 docker-compose rm

exec: ## sh into the php container
	docker exec -it php-fpm sh