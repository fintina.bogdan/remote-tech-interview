import './App.css';
import React, {useEffect, useState} from "react";
import axios from "axios";

function App() {
    const [parsedOptions, setParsedOptions] = useState([]);
    const [options, setOptions] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [selectedOption, setSelectedOption] = useState(null);
    const [uniqueColors, setUniqueColors] = useState(null);
    const [uniqueStorage, setUniqueStorage] = useState(null);
    const [selectedColor, setSelectedColor] = useState(null);
    const [selectedStorage, setSelectedStorage] = useState(null);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [selectedNetwork, setSelectedNetwork] = useState(null);
    const [selectedLiquids, setSelectedLiquids] = useState(null);
    const [selectedFunctional, setSelectedFunctional] = useState(null);
    const [selectedDisplay, setSelectedDisplay] = useState(null);
    const [selectedBack, setSelectedBack] = useState(null);
    const [price, setPrice] = useState(null);

    useEffect(() => {
        const fetchOptions = async () => {
            try {
                const response = await axios.get('http://localhost:81/products');
                const parsedOptions = response.data.map(option => ({
                    label: option.brand + " " + option.model,
                    value: option.id
                }));
                setParsedOptions(parsedOptions);
                setOptions(response.data);
            } catch (error) {
                console.error(error);
            }
        };
        fetchOptions();
    }, []);

    useEffect(() => {
        const calculatePrice = async () => {
            try {
                if (
                    selectedStorage !== null &&
                    selectedColor !== null &&
                    selectedProduct !== null &&
                    selectedNetwork !== null &&
                    selectedBack !== null &&
                    selectedDisplay !== null &&
                    selectedFunctional !== null &&
                    selectedLiquids !== null
                ) {
                    const response = await axios.post('http://localhost:81/product/price/calculate', {
                        storage: selectedStorage,
                        color: selectedColor,
                        product: selectedProduct,
                        network: selectedNetwork,
                        back: selectedBack,
                        display: selectedDisplay,
                        functional: selectedFunctional,
                        liquids: selectedLiquids,
                    });
                    setPrice(response.data.price);
                }
            } catch (error) {
                console.log(error)
            }
        };
        calculatePrice();
    }, [
        selectedStorage,
        selectedColor,
        selectedProduct,
        selectedNetwork,
        selectedBack,
        selectedDisplay,
        selectedFunctional,
        selectedLiquids
    ]);

    const filteredOptions = parsedOptions.filter(option =>
        option.label.toLowerCase().includes(searchTerm.toLowerCase())
    );

    const handleStorageClick = storage => {
        setSelectedStorage(storage)
    };

    const handleColorClick = color => {
        const uniqueStorage = new Set();
        Object.keys(selectedOption.productVariations).forEach(key => {
            if (selectedOption.productVariations[key].color === color) {
                uniqueStorage.add(selectedOption.productVariations[key].storage);
            }
        });

        setUniqueStorage(uniqueStorage)
        setSelectedColor(color)
        setSelectedStorage(null);
        setSelectedBack(null);
        setSelectedNetwork(null);
        setSelectedLiquids(null);
        setSelectedFunctional(null);
        setSelectedDisplay(null);
    };
    const handleSelect = event => {
        const selectedValue = event.target.value;
        const selectedOption = options.find(option => option.id == selectedValue);
        setSelectedOption(selectedOption);

        const uniqueColors = new Set();
        Object.keys(selectedOption.productVariations).forEach(key => {
            const color = selectedOption.productVariations[key].color;
            uniqueColors.add(color);
        });

        setUniqueColors(uniqueColors)
        setSelectedProduct(selectedValue)
        setSelectedColor(null)
        setSelectedStorage(null)
        setSelectedBack(null);
        setSelectedNetwork(null);
        setSelectedLiquids(null);
        setSelectedFunctional(null);
        setSelectedDisplay(null);
    };

    const handleClearSelection = () => {
        setSelectedOption(null);
        setUniqueColors(null);
        setUniqueStorage(null);
        setSelectedColor(null);
        setSelectedStorage(null);
        setSelectedProduct(null);
        setSelectedBack(null);
        setSelectedNetwork(null);
        setSelectedLiquids(null);
        setSelectedFunctional(null);
        setSelectedDisplay(null);
    };

    return (
        <div className="App">
            <div>
                {price && (
                    <div className={`row`}>
                        <p>Pret: {price} RON | £{Math.round(price / 5.55)} </p>
                    </div>
                )}
                <div className={`row`}>
                    <input type="text" placeholder="Search" onChange={event => setSearchTerm(event.target.value)}/>
                    <select value={selectedOption ? selectedOption.value : ''} onChange={handleSelect}>
                        <option value="">Select an option</option>
                        {filteredOptions.map(option => (
                            <option key={option.value} value={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </select>
                </div>
                {selectedOption && uniqueColors && (
                    <div className={`row`}>
                        <p>Culoare</p>
                        <div className={`right`}>
                            {Array.from(uniqueColors).map((color, index) => (
                                <button className={selectedColor === color ? "selected" : ""} key={`color-${index}`}
                                        onClick={() => handleColorClick(color)}>
                                    {color}
                                </button>
                            ))}
                        </div>
                    </div>
                )}
                {selectedOption && uniqueStorage && (
                    <div className={`row`}>
                        <p>Spatiu stocare</p>
                        <div className={`right`}>
                            {Array.from(uniqueStorage).map((storage, index) => (
                                <button className={selectedStorage === storage ? "selected" : ""}
                                        key={`storage-${index}`}
                                        onClick={() => handleStorageClick(storage)}>
                                    {storage} GB
                                </button>
                            ))}
                        </div>
                    </div>
                )}

                <div className={`row`}>
                    <p>Retea</p>
                    <div className={`right`}>
                        {Array.from(["Deblocat", "Orange", "Vodafone", "Telekom", "Operator strain"]).map((network, index) => (
                            <button className={selectedNetwork === network ? "selected" : ""} key={`network-${index}`}
                                    onClick={() => setSelectedNetwork(network)}>
                                {network}
                            </button>
                        ))}
                    </div>
                </div>
                <div className={`row`}>
                    <p>Lichide</p>
                    <div className={`right`}>
                        {Array.from(["Nu", "Da"]).map((liquid, index) => (
                            <button className={selectedLiquids === liquid ? "selected" : ""} key={`liquid-${index}`}
                                    onClick={() => setSelectedLiquids(liquid)}>
                                {liquid}
                            </button>
                        ))}
                    </div>
                </div>
                <div className={`row`}>
                    <p>Functional</p>
                    <div className={`right`}>
                        {Array.from(["Nu", "Da"]).map((functional, index) => (
                            <button className={selectedFunctional === functional ? "selected" : ""}
                                    key={`functional-${index}`}
                                    onClick={() => setSelectedFunctional(functional)}>
                                {functional}
                            </button>
                        ))}
                    </div>
                </div>
                <div className={`row`}>
                    <p>Ecran</p>
                    <div className={`right`}>
                        {Array.from(["Ca Nou", "Excelent", "Foarte Bun", "Bun", "Uzat"]).map((display, index) => (
                            <button className={selectedDisplay === display ? "selected" : ""} key={`display-${index}`}
                                    onClick={() => setSelectedDisplay(display)}>
                                {display}
                            </button>
                        ))}
                    </div>
                </div>
                <div className={`row`}>
                    <p>Lateral/Spate</p>
                    <div className={`right`}>
                        {Array.from(["Ca Nou", "Excelent", "Foarte Bun", "Bun", "Uzat"]).map((back, index) => (
                            <button className={selectedBack === back ? "selected" : ""} key={`back-${index}`}
                                    onClick={() => setSelectedBack(back)}>
                                {back}
                            </button>
                        ))}
                    </div>
                </div>
                <button onClick={handleClearSelection}>Clear selection</button>
            </div>
        </div>
    );
}

export default App;
