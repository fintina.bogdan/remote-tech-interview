## Remote Tech Interview Test Project

### Requirements
- have Docker installed on your system [How to install docker](https://docs.docker.com/get-docker/)
- have Make installed on your system

### Instalation:
- open the terminal and run `make up`
- Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### Make utilities:
`make up` - will run `docker-compose build`, `docker-compose up -d`, create the database, run migrations, populate the database and install dependencies

`make stop` - will run `docker-compose stop`

`make destroy` - will run `docker-compose rm`, destroy the volumes 

`make exec` - this will open a ssh connection for the application container. Use this to run various `npm` commands into the application container

### Todo:
- [ ] Add API Swagger docs
- [ ] Add conditions in the calculate logic to keep in mind the functional and liquids
- [ ] Add network values in the database and create an API endpoing to return them
- [ ] Add validation on CalculatePriceDto
- [ ] Normalize storage values in the frontend form (GB, TB, MB)
- [ ] Add style to the frontend
- [ ] add .dockerignore file

